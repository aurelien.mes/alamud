# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3

class cutWithEvent(Event3):
	NAME = "cut-With"
	
	def perform(self):
		if not self.object2.has_prop("cutable"):
			self.fail()
			return self.inform("cut-With.failed")
		self.inform("cut-With")
